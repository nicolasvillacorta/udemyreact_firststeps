// No necesito importar Component porque estoy usando una funcion,
// no una clase que extiende Component.
import React, { Component} from 'react'; 
// Si no importo esto explicitamente, no lo va a tomar.
import './Person.css';
import withClass from '../../../hoc/withClass';
// Instalando "npm install --save prop-types", puedo usar PropTypes. Se usa despues del corchete de cierre del componente.
import PropTypes from 'prop-types';

import Auxiliary from '../../../hoc/Auxiliary';
// Puedo crearlo igual que el App.js que ya esta creado, 
// pero voy a usar ES6. El componente puede ser una funcion que retorna
// JSX a fin de cuentas.
import AuthContext from '../../../context/auth-context';

class Person extends Component {

    // Al construir el componente, se crea la referencia. (Esto es propio de react 16 en adelante), inputElementRef se llama en el input
    // Habria que mirar aparte como se hace para tener refs en los componentes funcionales.
    constructor(props){
        super(props)
        this.inputElementRef = React.createRef();
    }

    // Con esta linea ya puedo acceder a "this.context...." y usar los valores del contexto en cualquier lado, no necesito wrappear nada en un componente AuthContext.Consumer como
    //  el ejemplo que comente abajo. (SOLO FUNCIONA EN COMPONENTES DE CLASE, en los funcionales tengo que usar un hook, "const authContext = useContext(AuthContext)" y
    //  ya tengo en la variable todos los valores. "authContext.login")
    static contextType = AuthContext;

    // Una vez montado el componente, se hace focus en el input, asi queda marcado.
    componentDidMount(){
        this.inputElementRef.current.focus();
        console.log(this.context.authenticated);
    }

    render() {
        console.log('[Person.js] rendering...')
        return (
                // React en las ultimas versiones ya tiene un componente "Auxiliary integrado, asi no debo crearlo yo", es React.Fragment.
                <Auxiliary>
                {/*<div className="Person" style={style}>*/}
                {/*<AuthContext.Consumer>
                    {(context) => context.authenticated ? <p>Authenticated!</p> : <p>Please log in.</p>}
                </AuthContext.Consumer>*/}
                {this.context.authenticated ? <p>Authenticated!</p> : <p>Please log in.</p>}
                <div>
                    <p onClick={this.props.click}>I'm {this.props.name} and I am {this.props.age} years old!</p>
                    <p>{this.props.children}</p> {/*Son los items que recibi dentro del tag.*/}
                    <input 
                        type="text" 
                        ref={this.inputElementRef}
                        onChange={this.props.changed} 
                        value={this.props.name} 
                    />
                </div>
                </Auxiliary>
        )
    }
   
}

// Sirve para documentar que tipo de props tiene el componente. Si yo haciendo esto, le pongo un String a la edad de alguna persona, va a funcionar bien a simple vista,
//  pero en consola me va a tirar que el prop esta mal, se espera un number y se obtuvo un string. Siempre es clave poner PropTypes.
Person.propTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    changed: PropTypes.func
}

export default withClass(Person, "Person");

/*
    CONVERTIR UN COMPONENTE FUNCIONAL EN DE CLASE
- Para ser de clase hay que importar tambien el {Component} de react.
- Hay que cambiar el "const person = (props) =>" por "class Person extends Component" y 
    los corchetes despues queda todo igual. (El export va a ser en mayuscula, porque ahora exporta la clase, no una variable)
- El return y todo lo que haga la funcion, ahora va a estar dentro del metodo render del componente. "render() { ....... }"
- Las props ahora no se acceden como variables de metodo, sino del constructor como variable de clase, por eso se llama con "this.props.name" en lugar de solo "props.name".
*/