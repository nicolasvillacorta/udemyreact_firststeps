import React, { PureComponent } from 'react';
import Person from './Person/Person';
import styled from 'styled-components';


// Cada styled devuelve siempre un componente de React
const StyledDiv =  styled.div` 
    width: 60%;
    margin: 16px auto;
    border: 1px solid #eee;
    box-shadow: 0 2px 3px #ccc;
    padding: 16px;
    text-align: center;

    @media (min-width: 500px){
    width: 450px;
    }`;

class Persons extends PureComponent {
    
    // static getDerivedStateFromProps(props, state){
    //     console.log('[Persons.js] getDerivedStateFromProps')
    //     return state;
    // }

    // componentWillReceiveProps(props){
    //     console.log('[Persons.js] componentWillReceiverProps', props);
    // }
    
    // Este metodo es clave, evita que se updatee el componente si no cambio nada en el, es decir, por ej, si lleno otro campo, "el de validacion por ej."
    // No va a re-renderizar el componente personas.
    // Si mi componente en lugar de extender Component, extiende PureComponent, automaticamente tiene esta clase overrideada con un props check completo, es decir, todo este if 
    //  esta implicito. El componente no updatea si todas las props siguen siendo iguales.
    /*shouldComponentUpdate(nextProps, nextState){
        
        console.log('[Persons.js] shouldComponentUpdate');
        if(
            nextProps.persons !== this.props.persons ||
            nextProps.changed !== this.props.changed ||
            nextProps.clicked !== this.props.clicked)
            return true;
        else
            return false;   
    }*/

    getSnapshotBeforeUpdate(prevProps, prevState){
        console.log('[Persons.js] getSnapshotBeforeUpdate');
        return null;
    }

    componentDidUpdate() {
        console.log('[Persons.js] componentDidUpdate')
    }

    render(){
        console.log('[Persons.js] rendering...')
        return this.props.persons.map((person, index) => {
           
            // El id (key) siempre tiene que estar en el outer element, por eso lo puse ahi (en el StyledDiv).
            return <StyledDiv  key = {person.id}><Person 
                click={() => this.props.clicked(index)}
                name={person.name} 
                age={person.age}       
                changed={(event) => this.props.changed(event, person.id)} 
                /></StyledDiv>
          });
        };
    }
    
export default Persons;