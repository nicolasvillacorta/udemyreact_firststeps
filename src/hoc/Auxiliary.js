
// La gracia de esta clase es ser un wrapper (lo use en Person.js) para retornar solo un elemento JSX, ya que todo el return debe devolver todo el contenido en un wrap.
// La carpeta se llama hoc por High Order Components (basicamente componentes que tienen dentro a otros componentes). 
// En windows no se puede llamar Aux.js el archivo. Tiene problemas cuando lo zipeo.

const aux = props => props.children;

export default aux;