import React from 'react';

// Este ejemplo es medio pavo, tranquilamente podria usar un <div className="App"... , pero meti este pasamanos por el curso.
/*const withClass = props => (

    <div className={props.classes}>
        {props.children}
    </div>

);*/
// Tambien se podria en el App hacer un export default withClass(App, className) y que esta clase sea una funcion que recibe WrappedComponent y className, esa es otra
// forma de crear este componente de mayor orden.
const withClass = (WrappedComponent, className) => {

    return props => (
        <div className={className}>
            {/*Sin esta linea: "{...props}" no tendrian las props en Person.js */}
            <WrappedComponent {...props}/>
        </div>


    );
}


export default withClass;