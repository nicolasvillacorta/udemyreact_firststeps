import React from 'react';

// El context es un un valor global de javascript. Tiene que wrappear a todas las partes de la aplicacion que podran acceder al contexto.
const authContext = React.createContext({

    authenticated: false,
    login: () => {}
    
});

export default authContext;