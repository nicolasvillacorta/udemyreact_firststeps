// Es clave tener la extension de chrome de React Developer tools. Sirve para debugear y mirar las dev tools del chrome.
import React, { Component } from 'react';
import './App.css';
// El radium es el paquete que tuve que instalar para darle capabilities de css, y el StyleRoot es un componente prediseñado que debe envolver a todo mi Componente root (App.js)
// para que funcione los css que el radium trae de cada componente, por ejemplo el que use en Persona. "npm install --save radium" Y despues agregarle aca el import y el export 
// abajo de todo para que funcione. (En cada componente que vaya a usar radium).
// import Radium, { StyleRoot } from 'radium';
// https://styled-components.com/, se descarga con "npm install --save styled-components". Tambien sirve para dar facilitar estilos.
import styled from 'styled-components';
import Persons from '../components/Persons/Persons';
import Validation from '../components/Validation/Validation';
import Auxiliary from '../hoc/Auxiliary';
import withClass from '../hoc/withClass';
import AuthContext from '../context/auth-context';


// En este boton escribo directamente CSS, no JS, por eso va el ';' al final de cada propiedad y van sin comillas sus values.
// Con el ${} le digo que los props que recibe el componente, generen contenido dinamico.
const StyledButton = styled.button`

        background-color: ${props => props.pressed ? 'red' : 'green'};
        font: inherit;
        border: 1x solid blue;
        padding: 8px;
        cursor: pointer;
        &:hover {
          background-color: ${props => props.pressed ? 'salmon' : 'lightgreen'};
          color: black

      }`;

// NOTA: EXiste algo llamado CSS Modules (https://github.com/css-modules/css-modules) que permite que los archivos .CSS apliquen a cada componente, por default en react los .CSS 
// aplican a todos los componentes.
class App extends Component {

  constructor(props){
    super(props);
    console.log('[App.js] constructor')
    // Antes el state se inicializaba aca, ahora se hace afuera.
  }

  state = {
    persons: [
      { id: 'asd1',name : 'Max', age : 28 },
      { id: 'asd2', name : 'Nico', age : 25},
      { id: 'asd3', name : 'Facu', age : 22}
    ], 
    otherState : 'some other value',
    showPersons : false,
    cantChars : null,
    changeCounter : 0,
    authenticated: false
  }

  static getDerivedStateFromProps(props, state){
    console.log('[App.js] getDerivedStateFromProps', props)
    return state;
  }

  // componentWillMount(){
  //   console.log('[App.js] componentWillMount')
  // }

  componentDidMount(){
    console.log('[App.js] componentDidMount')
  }

  shouldComponentUpdate(nextProps, nextState){
    console.log('[App.js] shouldComponnetUpdate')
    return true;
  }

  componentDidUpdate(){
    console.log('[App.js] componentDidUpdate')
  }

  loginHandler = () => {
    this.setState({authenticated : true})
  }
  nameChangedHandler = (event, id) => {

    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    }

    // const person = Object.assign({}, this.state.persons[personIndex]);

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

 
    // Este metodo es asyncronico, no esta confirmed que se ejecute INMEDIATAMENTE.
    /*this.setState(() => {
      persons : persons,
      changeCounter: this.state.changeCounter + 1,
    })*/
      // De esta manera es ideal cuando mi update del state depende de un state anterior (del changeCounter en este caso).
      this.setState((prevState, props) => {
      return {
      persons : persons,
      changeCounter: prevState.changeCounter + 1
      };
    });
  }

  deletePersonHandler = (personIndex) => {
    //const persons = this.state.persons.slice(); // slice copia el array y devuelve una copia, si dejo el puntero puede llevar a comportamiento indeseado.
    // Siempre es recomendable hacer una copia del state en lugar de manipularlo el mismo state.
    // Otra alternativa es usar el Spread de ES6
    const persons = [...this.state.persons]; // Agarra todos los elementos del array y los mete en una nuevo array.
    persons.splice(personIndex, 1);
    this.setState({persons: persons});
  }


  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow})
  }

 
  lengthListener = (event) => {
    const word = event.target.value;
    const cant = word.length;
    this.setState({cantChars : cant});
  }

  render() {
    // Esta es otra manera de dar estilo desde javascript, sin usar .css, pero tiene algunas restricciones.
    const style = {
      
    }
    
    console.log("[App.js] render.")
    let persons = null;


    if(this.state.showPersons){
      persons = (
        <div>
         <Persons 
          persons = {this.state.persons} 
          clicked={this.deletePersonHandler}
          changed={this.nameChangedHandler}
          />
         
          {/* Ejemplo hardcodeado 
          <Person 
            name={this.state.persons[0].name} 
            age={this.state.persons[0].age} /> 
          <Person
            name={this.state.persons[1].name} 
            age={this.state.persons[1].age}
            click={this.switchNameHandler.bind(this, 'Max!')}
            changed={this.nameChangedHandler}>My Hobbies: Racing</Person> 
          <Person
            name={this.state.persons[2].name} 
            age={this.state.persons[2].age} />*/}
        </div>
      );
      style.backgroundColor = 'red';
      // Como hover es un string, lo accedo asi con corchetes.
      style[':hover'] = {
        backgroundColor: 'salmon',
        color: 'black'
      }
    }

    //let classes = ['red', 'bold'].join(' '); // Esto convierte el array en el string: "red bold".
    var classes = [];
    if(this.state.persons.length <= 2){
      classes.push('red'); // classes = ['red']
    }
    if(this.state.persons.length <= 1){
      classes.push('bold'); // classes = ['red', 'bold']
    }

    return (
      //<StyleRoot> Lo comento porque ahora voy a sacar radium para usar styled components.
        <Auxiliary>
          <AuthContext.Provider 
            value={{
              authenticated: this.state.authenticated, 
              login: this.loginHandler}}>
            <h1>{this.props.appTitle}</h1>
            <p className={classes.join(' ')}>This is really working!</p>
            <StyledButton pressed={this.state.showPersons} onClick={this.togglePersonsHandler}>
              Toggle Persons
            </StyledButton>
            <StyledButton onClick={this.loginHandler}>
              Log in
            </StyledButton>
            {persons}
          </AuthContext.Provider>
              {/* Inicio del div
              <div> 
                this.state.showPersons ? 
                // Con el ternario puedo hacer que se muestre una cosa o null, los if dentro del JSX no funcionan.
                  <Person 
                    name={this.state.persons[0].name} 
                    age={this.state.persons[0].age} /> 
                  <Person
                    name={this.state.persons[1].name} 
                    age={this.state.persons[1].age}
                    click={this.switchNameHandler.bind(this, 'Max!')}
                    changed={this.nameChangedHandler}>My Hobbies: Racing</Person> 
                  <Person
                    name={this.state.persons[2].name} 
                    age={this.state.persons[2].age} /> {/* : null
              </div> Fin del div.*/}
          
          <hr></hr><h3>Segundo assignment</h3>
          <input onChange={(event) => this.lengthListener(event)}/><br></br>
          <p>{this.state.cantChars}</p>
          <Validation textLen={this.state.cantChars}/>
        </Auxiliary>
      //</StyleRoot>
    ); // Person se trae como un tag, porque es un componente. Lo identifica porque empieza con mayuscula.
    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Hi, I\'m a react app!!'))
  }
}

//export default Radium(App); Con radium lo hacia asi.
//export default App; 
export default withClass(App, "App");